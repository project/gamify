<?php

/**
 * @file
 * The gamify module.
 */

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\userpoints\Entity\UserPointsInterface;
use Drupal\gamify\Event\EntityEvent;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Render\Element;
use Drupal\user\UserInterface;

/**
 * Implements hook_theme().
 */
function gamify_theme() {
  return [
    'gamify_alert' => [
      'render element' => 'elements',
    ],
    'gamify_toolbar_user_link' => [
      'variables' => [
        'display_name' => '',
        'user_points' => '',
        'cache' => [],
      ],
    ],
  ];
}

/**
 * Implements hook_entity_base_field_info().
 *
 * Adds base field to the userpoints entity where recently received points
 * are saved, to allow reverse actions.
 */
function gamify_entity_base_field_info(EntityTypeInterface $entity_type) {
  if ($entity_type->id() === 'userpoints') {
    $fields = [];
    $fields['points'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Added quantity'))
      ->setDescription(t('Extend base field table with quantity to convenient sum points.'))
      ->setRequired(FALSE)
      ->setRevisionable(TRUE);
    return $fields;
  }
}

/**
 * Implements hook_ENTITY_TYPE_presave() for entity_view_display entities.
 *
 * Before saving we calculate value for field points from diff of original and
 * updated userpoints entity sum.
 *
 * @see gamify_entity_base_field_info()
 */
function gamify_userpoints_presave(UserPointsInterface $userpoints) {
  if ($userpoints->isNewRevision() && $userpoints->original instanceof UserPointsInterface) {
    $quantity = (int) $userpoints->get('quantity')->getString();
    $original_quantity = (int) $userpoints->original->get('quantity')->getString();
    $points = $quantity - $original_quantity;
    $userpoints->set('points', $points);
  }
}

/**
 * Implements hook_cron().
 *
 * @throws \Exception
 */
function gamify_cron() {
  // Always trigger init; will be stopped internal, if not wanted by config.
  \Drupal::service('gamify.watchdog_service')->initRun('gamify_cron');
  \Drupal::service('gamify.member_rise')->eval();
}

/**
 * Implements hook_entity_delete().
 */
function gamify_entity_delete(EntityInterface $entity): void {
  _gamify_entity_event($entity, EntityEvent::DELETE);
}

/**
 * Implements hook_entity_insert().
 */
function gamify_entity_insert(EntityInterface $entity): void {
  _gamify_entity_event($entity, EntityEvent::INSERT);
}

/**
 * Implements hook_entity_update().
 */
function gamify_entity_update(EntityInterface $entity): void {
  _gamify_entity_event($entity, EntityEvent::UPDATE);
}

/**
 * Emits entity event.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 * @param $event_id
 *
 * @return void
 */
function _gamify_entity_event(EntityInterface $entity, $event_id) {
  // Only handle content entities and ignore config entities.
  if ($entity instanceof ContentEntityInterface) {
    $entity_type_id = $entity->getEntityTypeId();
    $args = [$entity_type_id => $entity];
    if (isset($entity->original)) {
      $args[$entity_type_id . '_unchanged'] = $entity->original;
    }
    $event = new EntityEvent($entity, $args);
    $event_dispatcher = \Drupal::service('event_dispatcher');
    $event_dispatcher->dispatch($event, $event_id);
  }
}

/**
 * Prepares variables for gamify alert templates.
 *
 * Default template: gamify-alert.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the gamify alert information and any
 *     fields attached to the entity.
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_gamify_alert(array &$variables) {
  $variables['view_mode'] = $variables['elements']['#view_mode'];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

/**
 * Implements hook_user_cancel().
 */
function gamify_user_cancel($edit, UserInterface $account, $method) {
  switch ($method) {
    case 'user_cancel_block_unpublish':
      // Unpublish gamify alerts.
      $storage = \Drupal::entityTypeManager()->getStorage('gamify_alert');
      $gamify_alert_ids = $storage->getQuery()
        ->condition('uid', $account->id())
        ->condition('status', 1)
        ->execute();
      foreach ($storage->loadMultiple($gamify_alert_ids) as $gamify_alert) {
        $gamify_alert->set('status', FALSE);
        $gamify_alert->save();
      }
      break;

    case 'user_cancel_reassign':
      // Anonymize gamify alerts.
      $storage = \Drupal::entityTypeManager()->getStorage('gamify_alert');
      $gamify_alert_ids = $storage->getQuery()
        ->condition('uid', $account->id())
        ->execute();
      foreach ($storage->loadMultiple($gamify_alert_ids) as $gamify_alert) {
        $gamify_alert->setOwnerId(0);
        $gamify_alert->save();
      }
      break;
  }
}

/**
 * Implements hook_ENTITY_TYPE_predelete() for user entities.
 */
function gamify_user_predelete(UserInterface $account) {
  // Delete gamify alerts.
  $storage = \Drupal::entityTypeManager()->getStorage('gamify_alert');
  $gamify_alert_ids = $storage->getQuery()
    ->condition('uid', $account->id())
    ->execute();
  $gamify_alerts = $storage->loadMultiple($gamify_alert_ids);
  $storage->delete($gamify_alerts);
}

/**
 * Implements hook_preprocess_THEME() for gamify alerts.
 */
function gamify_preprocess_gamify_alert(array &$variables) {
  /** @var \Drupal\gamify\Entity\GamifyAlert $entity */
  $entity = $variables["elements"]["#gamify_alert"] ?? NULL;
  $variables['entity'] = $entity ?? NULL;
}
