# Gamify by User Points

Gamify ist ein Modul, um dem Arbeiten mit Drupal einen spielerischen Touch zu geben. User können Punkte sammeln und in spielerischer Konkurrenz ihrem Ehrgeiz freien Lauf lassen. Das soll zur lebhaften Aktivität auf der Applikation beitragen. - Auf der anderen Seite dient das Tool dazu, Prozesse auf der Seite sichtbar zu machen und wichtige Personen für die Interaktion auf der Seite zu identifizieren, zu belohnen und ihren guten Einfluss auf das Geschehen auszuweiten.

Der Leistungsumfang geht weit über das Belohnen für das Erstellen und Bearbeiten von Inhalten hinaus. Durch systematische Erstellung und Auswertung von Datenbank-Log-Einträgen können die verschiedensten Aktivitäten sichtbar gemacht werden. Sowohl positives als auch negatives Nutzer-Verhalten wie systematischer Missbrauch von Bewertungen kann identifiziert und somit auch vermieden werden.

## Datenbank Log-Messages

Gamify verwendet eine XML-ähnliche Log-Message-Notation, mit der jede Aktivität kurz und lesbar für Mensch und Maschine dargestellt werden kann. Was genau in der Log-Message stehen soll, wird pro Entity-Type über Hooks reguliert.

###  Beispiel:

Die einfachste Notation für eine Log-Message, die ein Voting-Event dokumentiert, sähe so aus.

````
[create][vote:209]
````

Was so zu lesen ist: Es wurde eine Entität vom Type 'vote' mit der ID '209' erstellt ('create'). Anhand des Logs können wir aber nichts weiter darüber sagen (abgesehen von den Header-Daten der Log-Message, wann und durch wen die Entität erstellt wurde).

Mit diesem Eintrag können wir immerhin feststellen, dass z.B. ein Nutzer in schneller Folge viele Bewertungen erstellt hat. Z.B. 10 Bewertungen in einer Minute.

Wenn der Nutzer Text-Beiträge (z.B. Argumente) bewertet hat, kann auf jeden Fall ausgeschlossen werden, dass er in einer Minute alle 10 Beiträge wirklich gelesen hat. In dem Fall ist die Bewertung als nicht-valide zu beurteilen, und man könnte daraus konsequenzen ziehen. - Wenn er allerdings Bilder bewertet hat, ist es sehr wohl möglich, dass er in einer Minute 10 Bilder bewertet hat. - Um die beiden Fälle voneinander unterscheiden zu können, brauchen wir mehr Information, z.B. den Type des Votings.

````
[create][vote:209 type='argument_relevance']
````

Noch besser wäre, wenn die Relation zur Entität, auf welche sich das Voting bezieht, direkt abgebildet wird. Das würde dann so aussehen.

````
[create][vote:209 type='argument_relevance'][argument:53]
````

Angenommen, ich habe einen Inhaltstyp 'rule', auf den sich Argumente vom Typ 'argument' beziehen. Argumente können Pro oder Contra sein, und jedes Argument kann einzeln bewertet und somit gewichtet werden. Jetzt möchte ich wissen, ob eine Art von betrügerischen Verhalten praktiziert wird, ob in schneller Folge alle Argumente vom Typ PRO positiv, und alle Argumente vom Typ CONTRA negativ bewertet werden.

Dazu benötige ich zusätzlich die Relation, auf die sich die Argumente beziehen, also den Inhalt vom Typ 'rule':

````
[create][vote:209 type='argument_relevance'][argument:53][node:9 bundle='rule']
````

Den Typ des Argumentes:
````
[create][vote:209 type='argument_relevance'][argument:53 type='contra'][node:9 bundle='rule']
````

Und letztlich die Qualität des Votings, ob diese gut oder schlecht ist:

````
[create][vote:209 quality='bad' type='argument_relevance'][argument:53 type='contra'][node:9 bundle='rule']
````

Mit diesen Informationen in den Log-Einträgen kann ich das schädliche Verhalten leicht und sehr schnell identifizieren, ohne dass Entitäten dazu geladen werden müssten.

````
[create][vote:209 quality='bad' type='argument_relevance'][argument:53 type='contra'][node:9 bundle='rule'][taxonomy_term:7 bundle='sections'][taxonomy_term:1 bundle='sections']
````



## Dependencies

- User Points (https://www.drupal.org/project/userpoints)
- Rules (https://www.drupal.org/project/rules)
- DbLog (Core)
- Views (Core)

## Features

- mit einer Vielzahl von bereitgestellten Rules-Plugins
  - Sammeln von User-Punkten (durch bereitgestellte Rules-Plugins)
  - Sammeln von Strafpunkten, die in einem


## Road map

- Installation von userpoints und gamify abschließen.

- Benchmark-System:
  - Benutzer sollen beim Erreichen von konfigurierbaren Userpoint-Marken Titel erwerben können.
  - Manche Titel sollen mit einer neuen Benutzerrolle verbunden werden.


- Alerts for users should display if user has already seen the detail view of the alert. if NOT
  If NOT a (NEW) batch should be displayed on the icon.
-
- Integration of Drupal Message:
  - https://www.drupal.org/project/message
  - https://www.drupal.org/project/private_message

- Improvement for user profile:
  - https://www.drupal.org/project/profile

### To be done

Folgende Tasks sollten umgesetzt werden, bevor Modul fertig ist:

1. Für das Data Reports in Gamify Alerts wurde der Text-Format-Filter full_html
   verwendet und mit Table field angereichert. Dafür muss es einen eigenen
   tabellenfähigen Text-Format-Filter geben.

1. Data Reports mit eigener Berechtigung versehen, damit User sich daraus keine
   Betrugsregeln ableiten können. (???)

1. Userpoint-Regeln für Change-Requests anlegen.

1. Neue Rules ReactionRule anlegen, mit Context: Serien-Elemente.
   - Votes löschen.
   - User blocken bzw. Rollen entziehen.

1. Regeln für Rulesfinder exportieren und verfügbar machen.

1. Installation rund machen.


### DONE (Features)

- User Points: Installed User Points module as dependency, patched and extended
  base fields for number of points added recently.

- Log Activities for gamify evaluation: Instead of an own log file system we
  used the watchdog to write log files of user activity.

- To prevent logs from being analysed several times, a system is required where
  already analysed data are excluded. (e.g. save a timestamp of the last loaded
  log entry.)

- Um Alerts, die in der Regel Standard-Texte enthalten, leichter erstellen und
  zentral Verwalten zu können, sollte es ein  Vorlagen-System dafür geben.

- Wenn ein User eine schlechte Bewertung abgibt, soll geprüft werden, ob es
  einen Kommentar (wenn möglich) gibt. Andernfalls soll der User auf die
  Notwendigkeit hingewiesen werden.

- Operation Hash verbessern:
  Der Slug, welcher als message für logs und User-Points verwendet wird, soll
  verbessert werden. Klare Zuordnung der Eigenschaften und technische wie auch
  kognitive Interpretierbarkeit.
- Alle Regeln überarbeiten, damit diese mit dem neuen Hash arbeiten.

- Die neue Action "Add Formatted String" (FormattedStringAdd) verwendet die
  Rules Action "EntityFetchById" diese musste jedoch angepasst werden, damit
  sowohl entity_type als auch entity_id dynamisch angegeben werden können.
  s. Rule message_bad_vote_requires_comment "Message BAD vote requires comment"

## Operation Hashes

Operation hashes are a kind of custom simplification dialect from the XML
structure, and resolves nested structure in a stringified way. It should give
the complete relevant context. - On the on hand it should be human read-able, on
the other hand convertable into a programmatic data structure (JSON, YAML or
nested arrays).

The first expression in a square bracket (XML: tag name) is a combination of
entity_type and entity id (node:7), and will be used in the entity type manager
to load entities.

#### Beispiel:

````
YAML:
_____
create:
  vote:202:
    type: argument_relevance
    quality: bad
    parent:
      argument:17:
        type: contra
        parent:
          node:3:
            bundle: rule
            parent:
              term:5
                name: work_order
                parent:
                  term:1
                    name: main

Correlating Action Hash:
[create][vote:202 type='argument_relevance' quality='bad'][argument:17 type='contra'][node:3 bundle='rule'][term:5 name='work_order'][term:1 name='main']
````
### ```/]``` vs. ```]```

A closing square bracket with a slash before means that the following element is a sibling; without slash, that the subsequent element is a child element. (It must be noted that the context specification goes from the inside out, so child elements must be interpreted as parents. See YAML notation above)
