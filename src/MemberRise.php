<?php

namespace Drupal\gamify;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\gamify\Entity\GamifyAlert;
use Drupal\userpoints\Service\UserPointsServiceInterface;

/**
 * Service description.
 */
class MemberRise {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The userpoints.points service.
   *
   * @var \Drupal\userpoints\Service\UserPointsServiceInterface
   */
  protected UserPointsServiceInterface $points;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * Drupal\gamify\AlertBuilderService definition.
   *
   * @var \Drupal\gamify\AlertBuilderService
   */
  protected AlertBuilderService $alertBuilder;

  /**
   * Constructs a MemberRise object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\userpoints\Service\UserPointsServiceInterface $points
   *   The userpoints.points service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger channel factory.
   * @param \Drupal\gamify\AlertBuilderService $alert_builder
   *   The gamify.alert_builder service to build alerts from templates.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, UserPointsServiceInterface $points, LoggerChannelFactoryInterface $logger, AlertBuilderService $alert_builder) {
    $this->config = $config_factory->get('gamify.settings');
    $this->alertBuilder = $alert_builder;
    $this->entityTypeManager = $entity_type_manager;
    $this->points = $points;
    $this->logger = $logger->get('gamify');
  }

  /**
   * Method description.
   */
  public function eval(): void {
    if (($min_user_points = $this->config->get('member_min') ?? 0) && $role = $this->config->get('role')) {
      try {
        $role_entity = $this->entityTypeManager->getStorage('user_role')->load($role);
        if (!!$role_entity) {
          /** @var \Drupal\user\UserInterface[] $users */
          $users = $this->entityTypeManager->getStorage('user')->loadByProperties(['status' => '1']);
          foreach ($users as $user) {
            if (!$user->hasRole($role)) {
              $user_points = (int) $this->points->getPoints($user, UserPointsLogService::DEFAULT_POINT_TYPE);
              if ($user_points >= $min_user_points) {
                $user->addRole($role);
                if ($user->save() === SAVED_UPDATED) {
                  $add_replacer = [
                    '@new_role' => (string) $role_entity->label(),
                    '@min_user_points' => $min_user_points,
                  ];
                  $this->alertBuilder->createAlertFromTemplate('congrats_new_member_role', $user, [], $add_replacer);
                }
              }
            }
          }
        }
      }
      catch (\Exception $e) {
        $this->logger->error($e->getMessage());
      }
    }
  }

}
