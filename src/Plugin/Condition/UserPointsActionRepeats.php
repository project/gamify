<?php

namespace Drupal\gamify\Plugin\Condition;

use Drupal\gamify\UserPointsLogService;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\rules\Core\RulesConditionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'User points max repeat' condition.
 *
 * @Condition(
 *   id = "user_points_action_repeats",
 *   label = @Translation("User points action max repeats"),
 *   category = @Translation("Gamify"),
 *   context_definitions = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Entity"),
 *       description = @Translation("Specifies the entity for which to evaluate the condition."),
 *       assignment_restriction = "selector"
 *     ),
 *     "type" = @ContextDefinition("string",
 *       label = @Translation("Action identifier"),
 *       description = @Translation("Action type identifier (create, update, ...) that is used in the following action."),
 *       options_provider = "\Drupal\gamify\TypedData\Options\EntityOperationOptions",
 *       assignment_restriction = "input"
 *     ),
 *     "repeats" = @ContextDefinition("integer",
 *       label = @Translation("Max repeats"),
 *       description = @Translation("Max number of repeats a user receive points for same action. Please note that repetitions are -1 less than the absolute number of the same process. 1 repetition means that the action can be performed first time +1 repeat = 2."),
 *     ),
 *   }
 * )
 */
class UserPointsActionRepeats extends RulesConditionBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\gamify\UserPointsLogService definition.
   *
   * @var \Drupal\gamify\UserPointsLogService
   */
  protected UserPointsLogService $userPointsLogService;

  /**
   * Constructs a UserHasEntityFieldAccess object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\gamify\UserPointsLogService $user_points_log_service
   *   User service loads th correct user from param.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, UserPointsLogService $user_points_log_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->userPointsLogService = $user_points_log_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('gamify.user_points_log_service')
    );
  }

  /**
   * Check if max number of repeats is reached, then it fails.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity to check for the provided field.
   * @param string $type
   *   Action type identifier (create, ...) used in the following action.
   * @param int $repeats
   *   The max number of repeats.
   *
   * @return bool
   *   TRUE if the current number of repeats is below or equal as allowed.
   */
  public function doEvaluate(FieldableEntityInterface $entity, string $type, int $repeats) {
    return $this->userPointsLogService->evalMaxRepeat($entity, $type, $repeats);
  }

}
