<?php

namespace Drupal\gamify\Plugin\Condition;

use Drupal\rules\Core\RulesConditionBase;

/**
 * Provides a 'Relative time from now' condition.
 *
 * @Condition(
 *   id = "gamify_relative_time",
 *   label = @Translation("Relative time from now"),
 *   category = @Translation("Data"),
 *   context_definitions = {
 *     "date" = @ContextDefinition("any",
 *       label = @Translation("Data to compare"),
 *       description = @Translation("The date to be compared, specified by using a data selector, e.g. 'node.created.value'."),
 *       assignment_restriction = "selector"
 *     ),
 *     "operation" = @ContextDefinition("string",
 *       label = @Translation("Operator"),
 *       description = @Translation("The comparison operator. Valid values are <, >."),
 *       assignment_restriction = "input",
 *       default_value = "<",
 *       options_provider = "\Drupal\rules\TypedData\Options\ComparisonOperatorOptions",
 *       required = FALSE
 *     ),
 *     "value" = @ContextDefinition("string",
 *       label = @Translation("Relative date value"),
 *       description = @Translation("The relative time value from now (e.g. '- 2 days') to be compared with data. See <a href='https://www.php.net/manual/de/datetime.formats.php#datetime.formats.relative' target='_blank'>PHP - Relative time formats</a> "),
 *       assignment_restriction = "input"
 *     ),
 *   }
 * )
 */
class RelativeTime extends RulesConditionBase {

  /**
   * Evaluate the data comparison.
   *
   * @param mixed $date
   *   Supplied date to test. Must be a timestamp (created, changed).
   * @param string $operation
   *   Date comparison operation. One of:
   *     - "<"
   *     - ">".
   * @param string $value
   *   The relative time value from now to be compared against $date.
   *   Valid are all strings.
   *
   * @return bool
   *   The evaluation of the condition.
   */
  protected function doEvaluate(mixed $date, string $operation, string $value): bool {
    $date = (int) $date;

    $now = \Drupal::time()->getRequestTime();
    $comparator = strtotime($value, $now);

    $operation = $operation ? strtolower($operation) : '<';
    if (in_array($operation, ['>', '>='])) {
      return ($date > $comparator);
    }
    elseif (in_array($operation, ['<', '<='])) {
      return $date < $comparator;
    }
    else {
      return FALSE;
    }
  }

}
