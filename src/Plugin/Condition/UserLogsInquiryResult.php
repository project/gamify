<?php

namespace Drupal\gamify\Plugin\Condition;

use Drupal\Core\Database\Connection;
use Drupal\gamify\Traits\GamifyDbLogTrait;
use Drupal\gamify\UserPointsLogService;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\rules\Core\RulesConditionBase;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * User db logs query.
 *
 * Load log entries from DB filtered by search strings on the log message
 * (log_hash). Multiple search conditions seperated by | pipe, negate condition
 * with ^ carot. E.g. "[update][node:|^bundle='rule'". RegEx not allowed here.
 *
 * @Condition(
 *   id = "user_logs_inquiry_result",
 *   label = @Translation("A. User DB logs query result"),
 *   category = @Translation("Gamify - cron logs inquiry"),
 *   weight = 5,
 *   context_definitions = {
 *     "pattern" = @ContextDefinition("string",
 *       label = @Translation("Search strings"),
 *       description = @Translation("Load log entries from DB, filtererd by time and by search strings on the log message (log_hash). Multiple search conditions seperated by | pipe, negate condition with ^ carot. E.g. [update][node:|^bundle='rule' - no RegEx here."),
 *       assignment_restriction = "input",
 *       required = FALSE
 *     ),
 *     "user" = @ContextDefinition("entity:user",
 *       label = @Translation("User entity"),
 *       description = @Translation("Specifies the target user for the inquiry. Leave empty to omit user filtering."),
 *       assignment_restriction = "selector",
 *       default_value = NULL,
 *       required = FALSE
 *     ),
 *     "time" = @ContextDefinition("integer",
 *       label = @Translation("Last run time"),
 *       description = @Translation("Defines the time of the last run. Enter '0' to not filter only recent events."),
 *       default_value = 0,
 *       required = FALSE
 *     ),
 *     "result" = @ContextDefinition("any",
 *       label = @Translation("Result object"),
 *       description = @Translation("The result to pass forward to action."),
 *       default_value = NULL,
 *       assignment_restriction = "selector"
 *     ),
 *     "prev_result_key" = @ContextDefinition("string",
 *       label = @Translation("Prev result key"),
 *       description = @Translation("Key for previous results as param for conditions based on it."),
 *       assignment_restriction = "input",
 *       default_value = NULL,
 *       required = FALSE
 *     ),
 *     "result_key" = @ContextDefinition("string",
 *       label = @Translation("Result key"),
 *       description = @Translation("Enter key for results as param for conditions based on it."),
 *       assignment_restriction = "input",
 *       default_value = NULL,
 *       required = FALSE
 *     )
 *   }
 * )
 */
class UserLogsInquiryResult extends RulesConditionBase implements ContainerFactoryPluginInterface {

  use GamifyDbLogTrait;

  /**
   * Drupal\gamify\UserPointsLogService definition.
   *
   * @var \Drupal\gamify\UserPointsLogService
   */
  protected UserPointsLogService $userPointsLogService;

  /**
   * The current active database's master connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * Constructs a UserHasEntityFieldAccess object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\gamify\UserPointsLogService $user_points_log_service
   *   User service loads th correct user from param.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, UserPointsLogService $user_points_log_service, Connection $database) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->userPointsLogService = $user_points_log_service;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('gamify.user_points_log_service'),
      $container->get('database')
    );
  }

  /**
   * Check if max number of repeats is reached, then it fails.
   *
   * @param string $pattern
   *   Regular expression for log message to filter log entries.
   * @param \Drupal\user\UserInterface|null $user
   *   The user to inquire.
   * @param int|null $time
   *   Defines the time of the last run. Leave empty to not filter only recent
   *   events.
   * @param object|null $result
   *   The result object where to save query result.
   * @param string|null $prev_result_key
   *   Key for previous results as param for conditions based on it.
   * @param string|null $result_key
   *   Enter key for results as param for follow-up conditions based on it.
   *
   * @return bool
   *   TRUE if the current number of repeats is below or equal as allowed.
   */
  public function doEvaluate(string $pattern, ?UserInterface $user, ?int $time, ?object $result, ?string $prev_result_key, ?string $result_key): bool {

    $query_result = $this->queryDbLog($pattern, $user, $time, TRUE);
    if (is_object($result) && $result_key) {
      $result->interim[$result_key] = $query_result;
    }

    return !!count($query_result);
  }

}
