<?php

namespace Drupal\gamify\Plugin\Condition;

use Drupal\gamify\Traits\DbLogResultTrait;
use Drupal\rules\Core\RulesConditionBase;

/**
 * Check length of results on multiple levels: min, medium and max.
 *
 * Uses result bins to get data and writes result to result object. Allows to
 * vary gradually the final action (different variations of action).
 *
 * @Condition(
 *   id = "leveled_result_length",
 *   label = @Translation("Z. Result min length (Division into 3 levels)"),
 *   category = @Translation("Gamify - cron logs inquiry"),
 *   weight = 99,
 *   context_definitions = {
 *     "result" = @ContextDefinition("any",
 *       label = @Translation("Result object"),
 *       description = @Translation("The result to pass forward to action."),
 *       assignment_restriction = "selector"
 *     ),
 *     "prev_result_key" = @ContextDefinition("string",
 *       label = @Translation("Prev result bin key"),
 *       description = @Translation("Results bin key, inquired for length."),
 *       assignment_restriction = "input"
 *     ),
 *     "key_match_bin" = @ContextDefinition("string",
 *       label = @Translation("Key match bin"),
 *       description = @Translation("Result bin key from where required params where distilled. Matching item will top the result list."),
 *       assignment_restriction = "input",
 *       default_value = NULL,
 *       required = FALSE
 *     ),
 *     "result_sum" = @ContextDefinition("boolean",
 *       label = @Translation("Sum all results"),
 *       description = @Translation("If Yes (TRUE), all result group are summed up and compared with length. If No (FALSE, default) length is compared with each result group."),
 *       options_provider = "\Drupal\rules\TypedData\Options\YesNoOptions",
 *       default_value = FALSE,
 *       required = FALSE
 *     ),
 *     "min_length" = @ContextDefinition("integer",
 *       label = @Translation("Min result length"),
 *       description = @Translation("Minimal length of result bin. Below this length, Condition returns false."),
 *       default_value = 15,
 *       assignment_restriction = "input"
 *     ),
 *     "med_length" = @ContextDefinition("integer",
 *       label = @Translation("Medium length"),
 *       description = @Translation("Min result length, but marks a higher level. Final action can take this into account. Set 0 to disable."),
 *       default_value = 0,
 *       assignment_restriction = "input"
 *     ),
 *     "max_length" = @ContextDefinition("integer",
 *       label = @Translation("Critical length"),
 *       description = @Translation("Result length, that marks the highest level. Final action can take this into account. Set 0 to disable."),
 *       default_value = 0,
 *       assignment_restriction = "input"
 *     ),
 *   }
 * )
 */
class LeveledResultLength extends RulesConditionBase {

  use DbLogResultTrait;

  /**
   * Check length of results in different levels (min, medium and max).
   *
   * @param object $result
   *   The result object passed forward to action.
   * @param string $prev_result_key
   *   Key for previous results as param for conditions based on it.
   * @param string $key_match_bin
   *   Result bin key from where required params where distilled. Matching item
   *   will top the result list.
   * @param bool $result_sum
   *   If No (FALSE, default) length is compared with each result group.
   *   If Yes (TRUE), all result group are summed up and compared with length.
   * @param int $min_length
   *   Minimal length of result bin. Below this length, Condition returns false.
   * @param int $med_length
   *   Med result length marks a higher level. Final action can take this into
   *   account (set 0 to disable).
   * @param int $max_length
   *   Result length marks highest level. Final action can take this into
   *   account. Set 0 to disable.
   *
   * @return bool
   *   TRUE if the current number of repeats is below or equal as allowed.
   */
  public function doEvaluate(object $result, string $prev_result_key, string $key_match_bin, bool $result_sum, int $min_length, int $med_length, int $max_length): bool {
    if ($prev_result_key && is_array($result->interim[$prev_result_key] ?? NULL)) {
      $length = 0;
      $abs_item_sum = 0;
      $data = [];
      $interim_result = $result->interim[$prev_result_key];
      if ($this->getNestingLevel($interim_result) >= 3) {

        // Multidimensional result.
        if ($key_match_bin && is_array($result->interim[$key_match_bin] ?? NULL)) {

          // Multidimensional results leaded by group item.
          foreach ($interim_result as $key => $item_group) {
            $item_count = count($item_group);
            $top_item = isset($result->interim[$key_match_bin][$key])
              ? [$key => $result->interim[$key_match_bin][$key]] : [];
            $data[$key] = array_merge($top_item, $item_group);
            $abs_item_sum += $item_count;
            if ($result_sum) {
              $length += $item_count;
            }
            else {
              $length = ($item_count > $length) ? $item_count : $length;
            }
          }
        }
        else {

          // Multidimensional results unleaded.
          foreach ($interim_result as $item_group) {
            $item_count = count($item_group);
            $data[] = $item_group;
            $abs_item_sum += $item_count;
            if ($result_sum) {
              $length = $length + $item_count;
            }
            else {
              $length = ($item_count > $length) ? $item_count : $length;
            }
          }
        }
      }
      else {
        // One dimensional result.
        $data[] = $result->interim[$prev_result_key];
        $abs_item_sum = $length = count($result->interim[$prev_result_key]);
      }
      // Set results.
      $result->abs_item_sum = $abs_item_sum;
      $result->length = $length;
      $result->data = $data;

      // Set level and Return condition.
      if ($max_length && $length >= $max_length) {
        $result->level = 'max';
        return TRUE;
      }
      elseif ($med_length && $length >= $med_length) {
        $result->level = 'med';
        return TRUE;
      }
      elseif ($length >= $min_length) {
        $result->level = 'min';
        return TRUE;
      }

    }

    return FALSE;
  }

}
