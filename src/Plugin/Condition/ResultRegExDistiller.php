<?php

namespace Drupal\gamify\Plugin\Condition;

use Drupal\rules\Core\RulesConditionBase;

/**
 * Uses RegEx to distill params from log_hash and save to result.
 *
 * This condition can be used to obtain parameters for later searches from
 * previous interim results. It saves the results as an array in the Result
 * object under a specified key so that conditions can access them later.
 *
 * @Condition(
 *   id = "result_regex_distiller",
 *   label = @Translation("B. Distill params by RegEx from result"),
 *   category = @Translation("Gamify - cron logs inquiry"),
 *   weight = 12,
 *   context_definitions = {
 *     "pattern" = @ContextDefinition("string",
 *       label = @Translation("RegEx pattern"),
 *       description = @Translation("Distill params from interim result and save to interim results. Use brackets in pattern to distill for requested argument."),
 *       assignment_restriction = "input"
 *     ),
 *     "result" = @ContextDefinition("any",
 *       label = @Translation("Result object"),
 *       description = @Translation("The result to pass forward to action."),
 *       assignment_restriction = "selector"
 *     ),
 *     "prev_result_key" = @ContextDefinition("string",
 *       label = @Translation("Prev result key"),
 *       description = @Translation("Key for the results, to inquire with pattern."),
 *       assignment_restriction = "input"
 *     ),
 *     "result_key" = @ContextDefinition("string",
 *       label = @Translation("Result key"),
 *       description = @Translation("Enter key for results as param for conditions based on it."),
 *       assignment_restriction = "input"
 *     )
 *   }
 * )
 */
class ResultRegExDistiller extends RulesConditionBase {

  /**
   * Check if max number of repeats is reached, then it fails.
   *
   * @param string $pattern
   *   Distill params from interim result and save to interim results. Use
   *   brackets in pattern to distill for requested argument.
   * @param object $result
   *   The result object where to save query result.
   * @param string $prev_result_key
   *   Key for previous results as param for conditions based on it.
   * @param string $result_key
   *   Enter key for results as param for follow-up conditions based on it.
   *
   * @return bool
   *   TRUE if the current number of repeats is below or equal as allowed.
   */
  public function doEvaluate(string $pattern, object $result, string $prev_result_key, string $result_key): bool {
    if ($prev_result_key && is_array($result->interim[$prev_result_key] ?? NULL)) {
      $interim_result = [];
      foreach ($result->interim[$prev_result_key] as $key => $prev_result) {
        $log_hash = $prev_result['log_hash'] ?? NULL;
        if (!is_string($log_hash)) {
          continue;
        }
        if (preg_match($pattern, $log_hash, $matches)) {
          unset($matches[0]);
          $interim_result[$key] = [
            'interim_stack' => $prev_result_key,
            'stack_id' => $key,
            'matches' => $matches,
            'entry' => $prev_result,
          ];
        }
      }
      if (count($interim_result)) {
        $result->interim[$result_key] = $interim_result;
        return TRUE;
      }
    }
    return FALSE;
  }

}
