<?php

namespace Drupal\gamify\Plugin\Condition;

use Drupal\gamify\Traits\DbLogResultTrait;
use Drupal\rules\Core\RulesConditionBase;

/**
 * Provides a 'Relative time from now' condition.
 *
 * @Condition(
 *   id = "frequent_repetitions_filter",
 *   label = @Translation("D. Find high frequent repetition series."),
 *   category = @Translation("Gamify - cron logs inquiry"),
 *   context_definitions = {
 *     "result" = @ContextDefinition("any",
 *       label = @Translation("Result object"),
 *       description = @Translation("The result object passed forward to action."),
 *       assignment_restriction = "selector"
 *     ),
 *     "prev_result_key" = @ContextDefinition("string",
 *       label = @Translation("Prev result key"),
 *       description = @Translation("Key for the results, to inquire for high frequent series."),
 *       assignment_restriction = "input"
 *     ),
 *     "result_key" = @ContextDefinition("string",
 *       label = @Translation("Result key"),
 *       description = @Translation("Enter results key for found series."),
 *       assignment_restriction = "input"
 *     ),
 *     "time_span" = @ContextDefinition("integer",
 *       label = @Translation("Max time span (seconds)"),
 *       description = @Translation("Max. time between two events in seconds (average) before the series breaks. The higher the value, the more events are combined into a series."),
 *       default_value = 10,
 *       assignment_restriction = "input"
 *     ),
 *     "length" = @ContextDefinition("integer",
 *       label = @Translation("Min. series length."),
 *       description = @Translation("Min. length of entriy items to be considered a series."),
 *       default_value = 3,
 *       assignment_restriction = "input"
 *     )
 *   }
 * )
 */
class FrequentRepetitionsFilter extends RulesConditionBase {

  use DbLogResultTrait;

  /**
   * Evaluate the data comparison.
   *
   * @param object $result
   *   The result object passed forward to action.
   * @param string $prev_result_key
   *   Key for the results, to inquire for high frequent series.
   * @param string $result_key
   *   Results key for found series.
   * @param int $time_span
   *   Max. time span between two events in seconds (average time difference).
   *   The higher the value, the more events are combined into a series.
   * @param int $length
   *   Minimum number of events before it can be considered a series.
   *
   * @return bool
   *   The evaluation of the condition.
   */
  protected function doEvaluate(object $result, string $prev_result_key, string $result_key, int $time_span, int $length): bool {
    $result->interim[$result_key] = [];
    if ($prev_result_key && is_array($result->interim[$prev_result_key] ?? NULL)) {
      $entries = $result->interim[$prev_result_key];
      // If prev_result is multiple array, we combine it to a single result stack.
      if ($this->getNestingLevel($entries) === 3) {
        $combined = [];
        foreach ($entries as $entry) {
          $combined = array_merge($combined, $entry);
        }
        $entries = $entries[array_key_first($entries)];
      }

      if (count($entries) >= $length) {
        $result->interim[$result_key] = $this->seriesSearch($entries, $time_span, $length);
      }
    }
    return !!count($result->interim[$result_key]);
  }

  /**
   * Search for continued series in given log entries by time interval.
   *
   * @param array $entries
   *   Entries to search.
   * @param int $range
   *   Max duration between 2 log entries. If time diff is greater than $range
   *   a new series opens.
   * @param int $min_length
   *   Minimum number of entries in a serial.
   *
   * @return array
   *   An array of all series found.
   */
  public function seriesSearch(array $entries, int $time_span = 60, int $min_length = 3): array {
    $series = [];
    $collector = [];
    $serial_begin = 0;
    foreach ($entries as $wid => $entry) {
      $current_timestamp = (int) $entry['timestamp'];
      // First entry.
      if (!count($collector)) {
        $collector[$wid] = $entry;
        $serial_begin = $current_timestamp;
        continue;
      }
      // Follow-up entry.
      $serial_duration = $current_timestamp - $serial_begin;
      $divisor = count($collector) - 1;
      if (($divisor == 0) || ($serial_duration / $divisor) <= $time_span) {
        $collector[$wid] = $entry;
      }
      else {
        // Close current collection and create a serial.
        if (count($collector) >= $min_length) {
          $series[] = $collector;
        }

        // Restart with new serial.
        $collector = [$wid => $entry];
        $serial_begin = $current_timestamp;
      }
    }
    if (count($collector) >= $min_length) {
      $series[] = $collector;
    }
    return $series;
  }

}
