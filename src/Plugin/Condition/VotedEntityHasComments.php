<?php

namespace Drupal\gamify\Plugin\Condition;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\rules\Core\RulesConditionBase;
use Drupal\votingapi\VoteInterface;

/**
 * Provides a 'vote quality result' condition.
 *
 * In vote_vote_type entities you can define if a voting is good/neutral/bad.
 * This condition gives the possibility to react on the result quality.
 *
 * @Condition(
 *   id = "voted_entity_has_comments",
 *   label = @Translation("Voted entity has comment field"),
 *   category = @Translation("Vote"),
 *   context_definitions = {
 *     "vote" = @ContextDefinition("any",
 *       label = @Translation("Vote entity"),
 *       description = @Translation("The vote entity, the user has created or updated."),
 *       assignment_restriction = "selector"
 *     ),
 *   }
 * )
 */
class VotedEntityHasComments extends RulesConditionBase {

  use StringTranslationTrait;

  /**
   * Check if voted entity type has a comment field.
   *
   * @param \Drupal\votingapi\VoteInterface $vote
   *   Supplied vote entity to evaluate by plugin.
   *
   * @see \Drupal\vote\TypedData\Options\VoteResultQuality
   *
   * @return bool
   *   The evaluation of the condition.
   */
  protected function doEvaluate(VoteInterface $vote): bool {
    if (\Drupal::moduleHandler()->moduleExists('comment')) {
      $voted_type = $vote->getVotedEntityType();

      /** @var \Drupal\comment\CommentManagerInterface $comment_manager */
      $comment_manager = \Drupal::service('comment.manager');
      if ($comment_fields = $comment_manager->getFields($voted_type)) {
        try {
          $voted_id = $vote->getVotedEntityId();
          $voted_entity = \Drupal::entityTypeManager()->getStorage($voted_type)->load($voted_id);
          $voted_bundle = $voted_entity->bundle();
          foreach ($comment_fields as $definition) {
            if (in_array($voted_bundle, $definition['bundles'])) {
              return TRUE;
            }
          }
        }
        catch (\Exception $e) {
          \Drupal::logger('vote')->error($e->getMessage());
        }
      }
    }
    return FALSE;
  }

}
