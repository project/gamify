<?php

namespace Drupal\gamify\Plugin\Condition;

use Drupal\rules\Core\RulesConditionBase;

/**
 * Uses RegEx on log_hash to filter interim results and save to result.
 *
 * You can use params from \Drupal\gamify\Plugin\Condition\ResultRegExDistiller.
 *
 * @Condition(
 *   id = "result_regex_filter",
 *   label = @Translation("C. Filter query result by RegEx (opt: with distilled params)."),
 *   category = @Translation("Gamify - cron logs inquiry"),
 *   weight = 10,
 *   context_definitions = {
 *     "pattern" = @ContextDefinition("string",
 *       label = @Translation("Search string/RegEx pattern"),
 *       description = @Translation("Filter interim result and save to final or interim results. Use slash / as delimiter at begin+end to use regex. Use $1, $2, ... to use params from ResultRegExDistiller."),
 *       assignment_restriction = "input"
 *     ),
 *     "result" = @ContextDefinition("any",
 *       label = @Translation("Result object"),
 *       description = @Translation("The result to pass forward to action."),
 *       assignment_restriction = "selector"
 *     ),
 *     "prev_result_key" = @ContextDefinition("string",
 *       label = @Translation("Prev result key"),
 *       description = @Translation("Key for the results, to inquire with pattern."),
 *       assignment_restriction = "input"
 *     ),
 *     "prev_params_key" = @ContextDefinition("string",
 *       label = @Translation("Prev params key"),
 *       description = @Translation("Key where params to use here are stored."),
 *       assignment_restriction = "input",
 *       default_value = NULL,
 *       required = FALSE
 *     ),
 *     "result_key" = @ContextDefinition("string",
 *       label = @Translation("Result key"),
 *       description = @Translation("Enter key for interem results. If empty the result will be the final data result."),
 *       assignment_restriction = "input"
 *     )
 *   }
 * )
 */
class ResultRegExFilter extends RulesConditionBase {

  /**
   * Check if max number of repeats is reached, then it fails.
   *
   * @param string $pattern
   *   Distill params from interim result and save to interim results. Use
   *   brackets in pattern to distill for requested argument.
   * @param object $result
   *   The result object where to save query result.
   * @param string $prev_result_key
   *   Key for previous results as param for conditions based on it.
   * @param string $result_key
   *   Enter key for results as param for follow-up conditions based on it.
   *
   * @return bool
   *   TRUE if the current number of repeats is below or equal as allowed.
   */
  public function doEvaluate(string $pattern, object $result, string $prev_result_key, string $prev_params_key, string $result_key): bool {
    if ($prev_result_key && is_array($result->interim[$prev_result_key] ?? NULL)) {
      $interim_result = [];

      // Check if we have to work with params from ResultRegExDistiller.
      if ($prev_params_key && is_array($result->interim[$prev_params_key] ?? NULL)) {

        // Using params => +1 loop more because have to handle each param set.
        foreach ($result->interim[$prev_params_key] as $params_key => $params) {
          $matches = $params['matches'] ?? NULL;
          if ($matches && $prepared_pattern = $this->preparePattern($pattern, $matches)) {
            if ($search_result = $this->searchEntries($result->interim[$prev_result_key], $prepared_pattern)) {
              $interim_result[$params_key] = $search_result;
            }
          }
        }
      }
      elseif ($prepared_pattern = $this->preparePattern($pattern, [])) {
        if ($search_result = $this->searchEntries($result->interim[$prev_result_key], $prepared_pattern)) {
          $interim_result[] = $search_result;
        }
      }
      // Handle result.
      if ($result_key) {
        $result->interim[$result_key] = $interim_result;
      }
      else {
        // @todo Put result to data.
      }
      return !!count($interim_result);
    }
    return FALSE;
  }

  /**
   * Prepare search pattern.
   *
   * @param string $pattern
   *   The search pattern, optional with placeholders ($1, $2, ...) for params.
   * @param array $params
   *   The params are the match result from a ResultRegExDistiller.
   *
   * @return mixed
   *   Prepared search pattern.
   */
  protected function preparePattern(string $pattern, array $params): mixed {
    $is_regex = (str_starts_with($pattern, '/') && str_ends_with($pattern, '/'));
    if (count($params)) {
      $replacer = [];
      foreach ($params as $key => $param) {
        $replacer["$$key"] = ($is_regex) ? preg_quote($param) : $param;
      }
      $pattern = str_replace(array_keys($replacer), $replacer, $pattern);
    }
    if ($is_regex && @preg_match($pattern, NULL) === FALSE) {
      return FALSE;
    }
    return ($is_regex)
      ? $pattern
      : explode('|', $pattern);
  }

  /**
   * Searches for entries, where log_hash matching the pattern.
   *
   * @param array[] $entries
   *   Entries from log with at least a log_hash property.
   * @param mixed $pattern
   *   Search pattern. RegEx OR array of search strings where each string must
   *   match the log hash of an entry.
   *
   * @return array|null
   *   Result array of entries filtered by the pattern.
   */
  protected function searchEntries(array $entries, mixed $pattern): ?array {
    $result = [];
    foreach ($entries as $result_key => $prev_result) {
      $matched = FALSE;
      // If preparePattern returns array => String search.
      if (is_array($pattern)) {
        foreach ($pattern as $str) {
          $matched = str_contains($prev_result['log_hash'], $str);
          if (!$matched) {
            break;
          }
        }
      }
      // If preparePattern returns string => RegEx.
      elseif (is_string($pattern)) {
        $matched = preg_match($pattern, $prev_result['log_hash']);
      }
      if ($matched) {
        $result[$result_key] = $prev_result;
      }
    }
    return count($result) ? $result : NULL;
  }

}
