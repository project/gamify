<?php

namespace Drupal\gamify\Plugin\RulesAction;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\rules\Core\RulesActionBase;
use Drupal\gamify\UserPointsLogService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Revert user points action' action.
 *
 * @RulesAction(
 *   id = "revert_user_points",
 *   label = @Translation("Revert user points"),
 *   category = @Translation("Gamify"),
 *   context_definitions = {
 *     "type" = @ContextDefinition("string",
 *       label = @Translation("Action identifier"),
 *       description = @Translation("Action identifier that will be written to user points log message."),
 *       options_provider = "\Drupal\gamify\TypedData\Options\EntityOperationOptions",
 *       assignment_restriction = "input"
 *     ),
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Entity"),
 *       description = @Translation("Specifies the entity that was created."),
 *       assignment_restriction = "selector"
 *     ),
 *     "addressed_user" = @ContextDefinition("string",
 *       label = @Translation("Addressed user"),
 *       description = @Translation("User who will receive user points."),
 *       options_provider = "\Drupal\gamify\TypedData\Options\AbstractUserOptions",
 *       assignment_restriction = "input"
 *     ),
 *   }
 * )
 */
class RevertUserPoints extends RulesActionBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\gamify\UserPointsLogService definition.
   *
   * @var \Drupal\gamify\UserPointsLogService
   */
  protected UserPointsLogService $userPointsLogService;

  /**
   * Constructs an EntityCreate object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\gamify\UserPointsLogService $user_points_log_service
   *   User service loads th correct user from param.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, UserPointsLogService $user_points_log_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->userPointsLogService = $user_points_log_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('gamify.user_points_log_service')
    );
  }

  /**
   * Executes the Plugin.
   *
   * @param string $type
   *   Original value of an element which is being updated.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that should get an alias.
   * @param string $addressed_user
   *   The abstract user who will receive user points.
   */
  protected function doExecute(string $type, EntityInterface $entity, string $addressed_user): void {
    $this->userPointsLogService->revertUserPoints($type, $entity, $addressed_user);
  }

}
