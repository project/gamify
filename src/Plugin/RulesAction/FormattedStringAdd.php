<?php

namespace Drupal\gamify\Plugin\RulesAction;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\rules\Core\RulesActionBase;

/**
 * Provides an 'Add a formatted string' action.
 *
 * @todo The context definition for "type" needs an options_provider list.
 *
 * @RulesAction(
 *   id = "add_formatted_string",
 *   label = @Translation("Add a formatted string"),
 *   category = @Translation("Data"),
 *   context_definitions = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Entity"),
 *       description = @Translation("Specifies the entity that delivers the data."),
 *       assignment_restriction = "selector"
 *     ),
 *     "unformatted" = @ContextDefinition("string",
 *       label = @Translation("Unformatted value"),
 *       description = @Translation("Unformatted string with placeholders (e.g. @id @label) to be replaced with data from entity."),
 *       required = FALSE
 *     ),
 *   },
 *   provides = {
 *     "formatted_string" = @ContextDefinition("string",
 *       label = @Translation("Formatted string")
 *     ),
 *   }
 * )
 */
class FormattedStringAdd extends RulesActionBase {
  use StringTranslationTrait;

  /**
   * Add a variable.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The data type the new variable is of.
   * @param string $unformatted
   *   The variable to add.
   */
  protected function doExecute(EntityInterface $entity, string $unformatted): void {
    if (preg_match_all('/([@%][a-z_]+)/i', $unformatted, $matches)) {
      $replacer = [];
      foreach ($matches[1] as $placeholder) {
        $field_name = substr($placeholder, 1);
        if (method_exists($entity, $field_name)) {
          $replacer[$placeholder] = $entity->$field_name();
        }
        elseif ($entity->hasField($field_name) && ($value = $entity->get($field_name) ?? NULL)) {
          $replacer[$placeholder] = $value->getString();
        }
      }
      $formatted = $this->t($unformatted, $replacer)->render();
    }
    else {
      $formatted = $unformatted;
    }
    $this->setProvidedValue('formatted_string', $formatted);
  }

  /**
   * {@inheritdoc}
   */
  public function refineContextDefinitions(array $selected_data): void {
    $this->pluginDefinition['provides']['formatted_string']->setDataType('string');
  }

}
