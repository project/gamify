<?php

namespace Drupal\gamify\Plugin\RulesAction;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\gamify\AlertBuilderService;
use Drupal\rules\Core\RulesActionBase;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Set user points action' action.
 *
 * @RulesAction(
 *   id = "create_alert",
 *   label = @Translation("B. Create alert"),
 *   category = @Translation("Gamify - cron logs inquiry"),
 *   context_definitions = {
 *     "user" = @ContextDefinition("entity:user",
 *       label = @Translation("The target user"),
 *       description = @Translation("The inqueried user."),
 *       assignment_restriction = "selector"
 *     ),
 *     "template" = @ContextDefinition("string",
 *       label = @Translation("Alert template"),
 *       description = @Translation("Template to use for messages."),
 *       options_provider = "\Drupal\gamify\TypedData\Options\AlertTemplateOptions"
 *     ),
 *     "overwrites" = @ContextDefinition("any",
 *       label = @Translation("Overwrites"),
 *       description = @Translation("Array with data to overwrite template data."),
 *       default_value = NULL,
 *       required = FALSE
 *     ),
 *     "replacers" = @ContextDefinition("any",
 *       label = @Translation("Additional replacers"),
 *       description = @Translation("Values that will replace the placeholders in label and message ()."),
 *       default_value = NULL,
 *       required = FALSE
 *     ),
 *   }
 * )
 */
class CreateAlert extends RulesActionBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\gamify\AlertBuilderService definition.
   *
   * @var \Drupal\gamify\AlertBuilderService
   */
  protected AlertBuilderService $alertBuilder;

  /**
   * Constructs an EntityCreate object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\gamify\AlertBuilderService $alert_builder
   *   The alert builder service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AlertBuilderService $alert_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->alertBuilder = $alert_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('gamify.alert_builder')
    );
  }

  /**
   * Create Alerts for all delivered series.
   *
   * @param \Drupal\user\UserInterface $user
   *   Supplied date to test. Must be a timestamp (created, changed).
   * @param string $template
   *   The template to use for message.
   * @param mixed $overwrites
   *   List to be searched.
   * @param mixed $replacers
   *   List to be searched.
   */
  protected function doExecute(UserInterface $user, string $template, $overwrites, $replacers): void {
    $overwrites = (is_array($overwrites)) ? $overwrites : [];
    $replacers = (is_array($replacers)) ? $replacers : [];
    $this->alertBuilder->createAlertFromTemplate($template, $user, $overwrites, $replacers);
  }

}
