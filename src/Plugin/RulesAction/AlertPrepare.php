<?php

namespace Drupal\gamify\Plugin\RulesAction;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\gamify\DataReportCreator;
use Drupal\rules\Core\RulesActionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Data set' action.
 *
 * @RulesAction(
 *   id = "alert_prepare",
 *   label = @Translation("A. Prepare alert"),
 *   description = @Translation("Prepares Gamify alert by mapping alert template to levels and creating a HTML table from data report (provided as context variable)."),
 *   category = @Translation("Gamify - cron logs inquiry"),
 *   context_definitions = {
 *     "result" = @ContextDefinition("any",
 *       label = @Translation("Series result"),
 *       description = @Translation("Series result (provided by WatchdogSeries) <object>."),
 *       allow_null = TRUE,
 *       assignment_restriction = "selector"
 *     ),
 *     "min_template" = @ContextDefinition("string",
 *       label = @Translation("Alert template"),
 *       description = @Translation("Template to use for alert if level is 'min'."),
 *       options_provider = "\Drupal\gamify\TypedData\Options\AlertTemplateOptions"
 *     ),
 *     "med_template" = @ContextDefinition("string",
 *       label = @Translation("Critical template"),
 *       description = @Translation("Template to use for alert if level is 'medium'."),
 *       options_provider = "\Drupal\gamify\TypedData\Options\AlertTemplateOptions"
 *     ),
 *     "max_template" = @ContextDefinition("string",
 *       label = @Translation("Critical template"),
 *       description = @Translation("Template to use for alert if level is 'max'."),
 *       options_provider = "\Drupal\gamify\TypedData\Options\AlertTemplateOptions"
 *     ),
 *   },
 *   provides = {
 *     "msg_template" = @ContextDefinition("string",
 *       label = @Translation("Message template")
 *     ),
 *     "msg_overwrites" = @ContextDefinition("any",
 *       label = @Translation("Message overwrites")
 *     ),
 *     "msg_replacers" = @ContextDefinition("any",
 *       label = @Translation("Message replacers")
 *     ),
 *   }
 * )
 */
class AlertPrepare extends RulesActionBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\gamify\DataReportCreator definition.
   *
   * @var \Drupal\gamify\DataReportCreator
   */
  protected DataReportCreator $reportCreator;

  /**
   * Constructs an EntityCreate object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\gamify\DataReportCreator $report_creator
   *   User service loads th correct user from param.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, DataReportCreator $report_creator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->reportCreator = $report_creator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('gamify.data_report_creator')
    );
  }

  /**
   * Provides variables from series rusult to prepare an alert.
   *
   * @param mixed $result
   *   The series result object provided by WatchdogSeries.
   *   Template to use if level is min.
   * @param string $min_template
   *   Template to use if level is min.
   * @param string $med_template
   *   Template to use if level is med.
   * @param string $max_template
   *   Template to use if level is max.
   */
  protected function doExecute(object $result, string $min_template, string $med_template, string $max_template): void {
    $this->setProvidedValue('msg_template', $this->getTemplate($result, $min_template, $med_template, $max_template));
    $this->setProvidedValue('msg_overwrites', $this->getOverwrites($result));
    $this->setProvidedValue('msg_replacers', $this->getReplacers($result));
  }

  /**
   * Provides the template to use.
   *
   * No test required, if the template still exists, because we have typed data,
   * and finally it is tested in the CreateAlert action.
   *
   * @param mixed $result
   *   The series result object provided by WatchdogSeries.
   * @param string $min_template
   *   Template to use if level is min.
   * @param string $med_template
   *   Template to use if level is med.
   * @param string $max_template
   *   Template to use if level is max.
   *
   * @return string
   *   Template to use for the alert.
   */
  protected function getTemplate(object $result, string $min_template, string $med_template, string $max_template): string {
    $template = $min_template;
    if (isset($result->level)) {
      if ($result->level == 'med') {
        $template = $med_template;
      }
      elseif ($result->level == 'max') {
        $template = $max_template;
      }
    }
    return $template;
  }

  /**
   * Overwrites for creating an Alert.
   *
   * @param object $result
   *   The series result object provided by WatchdogSeries.
   *
   * @return array
   *   Array with planed overwrites.
   */
  protected function getOverwrites(object $result): array {
    $data_report = [];
    if (isset($result->data)) {
      foreach ($result->data as $data) {
        $data_report[] = $this->reportCreator->createDataReport($data) ?? '';
      }
    }
    $data_report = implode("\n", $data_report);
    return ($data_report !== '') ? [
      'data_report' => $data_report,
    ] : [];
  }

  /**
   * Replacers for label and message when creating an Alert.
   *
   * @param object $result
   *   The series result object provided by WatchdogSeries.
   *
   * @return array
   *   Array with replacers. E.g. ['%series_count' => "10 items"]
   */
  protected function getReplacers(object $result): array {
    $replacers = [];
    foreach ($result as $key => $value) {
      if (is_string($value) || is_numeric($value)) {
        $replacers["%$key"] = (string) $value;
        $replacers["@$key"] = (string) $value;
      }
    }
    return $replacers;
  }

}
