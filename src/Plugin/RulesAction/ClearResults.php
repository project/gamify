<?php

namespace Drupal\gamify\Plugin\RulesAction;

use Drupal\gamify\Traits\DbLogResultTrait;
use Drupal\rules\Core\RulesActionBase;

/**
 * Clear result object.
 *
 * When all conditions and actions are evaluated, the results must be cleared,
 * to not abduct result properties to next rule evaluation.
 *
 * @RulesAction(
 *   id = "clear_result",
 *   label = @Translation("Z. Clear result object"),
 *   category = @Translation("Gamify - cron logs inquiry"),
 *   context_definitions = {
 *     "result" = @ContextDefinition("any",
 *       label = @Translation("Result object"),
 *       description = @Translation("The result to be cleared."),
 *       assignment_restriction = "selector"
 *     )
 *   }
 * )
 */
class ClearResults extends RulesActionBase {

  use DbLogResultTrait;

  /**
   * Create Alerts for all delivered series.
   *
   * @param object $result
   *   Result to be cleared.
   */
  protected function doExecute(object $result): void {
    $this->clearResult($result);
  }

}
