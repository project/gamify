<?php

namespace Drupal\gamify;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Swaps out the core condition manager.
 */
class GamifyServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container): void {
    // Overrides the core user toolbar link builder with our own.
    $definition = $container->getDefinition('user.toolbar_link_builder');
    $definition->setClass(GamifyUserLinkBuilder::class);
    $definition->addArgument(new Reference('userpoints.points'));
  }

}
