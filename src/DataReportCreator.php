<?php

namespace Drupal\gamify;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\Element\Datetime;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Service description.
 */
class DataReportCreator {

  use StringTranslationTrait;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Drupal\Core\Datetime\DateFormatterInterface definition.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * Constructs a DataReportCreator object.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service from core.
   */
  public function __construct(RendererInterface $renderer, DateFormatterInterface $date_formatter) {
    $this->renderer = $renderer;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * Creates a data table from received serial report.
   *
   * Table will be rendered and injected as HTML to text_format field.
   *
   * @param object $serial
   *   The serial.
   *
   * @return string|null
   *   Rendered HTML string to be injected to text_format field.
   */
  public function createSerialReport(object $serial): ?string {
    if (isset($serial->logs)) {
      $header = [
        'log' => $this->t('Log Id'),
        'user' => $this->t('User Id'),
        'content' => $this->t('Operation Hash'),
        'time' => $this->t('Time'),
      ];
      $rows = [];
      foreach ($serial->logs as $logId => $entry) {
        $rows[] = [
          'log' => $logId,
          'user' => $entry->uid ?? '',
          'content' => $entry->message ?? '',
          'time' => $this->dateFormatter->format($entry->timestamp, 'custom', 'H:i:s'),
        ];
      }

      $build['table'] = [
        '#type' => 'table',
        '#caption' => $this->t('<p>Series of @num items on @date that takes @dur sec:</p>', [
          '@num' => $serial->count,
          '@date' => $this->dateFormatter->format($serial->begin, 'custom', 'd/m/Y'),
          '@dur' => $serial->duration,
        ]),
        '#header' => $header,
        '#rows' => $rows,
        '#empty' => $this->t('No content has been found.'),
      ];
      try {
        $wrap_up = (string) $this->renderer->renderPlain($build);
      }
      catch (\Exception $e) {
        \Drupal::logger('gamify')->error($e->getMessage());
      }
    }
    return $wrap_up ?? NULL;
  }

  /**
   * Creates a data table from received serial report.
   *
   * Table will be rendered and injected as HTML to text_format field.
   *
   * @param array $data
   *   The serial.
   *
   * @return string|null
   *   Rendered HTML string to be injected to text_format field.
   */
  public function createDataReport(array $data): ?string {
    if ($count = count($data)) {
      $header = [
        'log' => $this->t('Log Id'),
        'user' => $this->t('User Id'),
        'content' => $this->t('Operation Hash'),
        'time' => $this->t('Time'),
      ];
      $rows = [];
      $t_min = time();
      $t_max = 0;
      foreach ($data as $logId => $entry) {
        if ($timestamp = $entry['timestamp'] ?? 0) {
          $t_min = ($timestamp < $t_min) ? $timestamp : $t_min;
          $t_max = ($timestamp > $t_max) ? $timestamp : $t_max;
        }

        $rows[] = [
          'log' => "{$entry['src']}-{$entry['log_id']}",
          'user' => $entry['uid'] ?? '',
          'content' => $entry['log_hash'] ?? '',
          'time' => ($timestamp) ? $this->dateFormatter->format($timestamp, 'custom', 'H:i:s') : '',
        ];
      }

      $duration = (new \DateTime())->setTimestamp($t_min);
      $duration = $duration->diff((new \DateTime())->setTimestamp($t_max));

      $build['table'] = [
        '#type' => 'table',
        '#caption' => $this->t('<p>Data set of @num items begins on @date and ends after @dur:</p>', [
          '@num' => $count,
          '@date' => $this->dateFormatter->format($t_min, 'custom', 'd/m/Y H:i'),
          '@dur' => $duration->format('%Hh %Im %Ss'),
        ]),
        '#header' => $header,
        '#rows' => $rows,
        '#empty' => $this->t('No data given.'),
      ];
      try {
        $wrap_up = (string) $this->renderer->renderPlain($build);
      }
      catch (\Exception $e) {
        \Drupal::logger('gamify')->error($e->getMessage());
      }
    }
    return $wrap_up ?? NULL;
  }

}
