<?php

namespace Drupal\gamify;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of alert_templates.
 */
class AlertTemplateListBuilder extends ConfigEntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['type'] = $this->t('Type');
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\gamify\AlertTemplateInterface $entity */
    $row['type'] = $this->t('<i class="argue-icon" aria-hidden="true">@type</i>', ['@type' => $entity->get('type')]);
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    return $row + parent::buildRow($entity);
  }

}
