<?php

namespace Drupal\gamify\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an alert_template entity type.
 */
interface AlertTemplateInterface extends ConfigEntityInterface {

}
