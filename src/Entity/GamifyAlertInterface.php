<?php

namespace Drupal\gamify\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a gamify alert entity type.
 */
interface GamifyAlertInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
