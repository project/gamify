<?php

namespace Drupal\gamify\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\EntityOwnerTrait;
use Drupal\gamify\TypedData\Options\AlertTypeOptions;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines the gamify alert entity class.
 *
 * @ContentEntityType(
 *   id = "gamify_alert",
 *   label = @Translation("Gamify alert"),
 *   label_collection = @Translation("Gamify alerts"),
 *   label_singular = @Translation("Gamify alert"),
 *   label_plural = @Translation("Gamify alerts"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Gamify alerts",
 *     plural = "@count Gamify alerts",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\gamify\GamifyAlertListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\gamify\GamifyAlertAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\gamify\Form\GamifyAlertForm",
 *       "edit" = "Drupal\gamify\Form\GamifyAlertForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "gamify_alert",
 *   admin_permission = "administer gamify alert",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/reports/gamify-alert",
 *     "add-form" = "/admin/reports/gamify-alert/add",
 *     "canonical" = "/admin/reports/gamify-alert/{gamify_alert}",
 *     "edit-form" = "/admin/reports/gamify-alert/{gamify_alert}/edit",
 *     "delete-form" = "/admin/reports/gamify-alert/{gamify_alert}/delete",
 *   },
 * )
 */
class GamifyAlert extends ContentEntityBase implements GamifyAlertInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;
  use StringTranslationTrait;

  const NOTE = 'note';
  const WARNING = 'warning';
  const ERROR = 'error';
  const REWARD = 'reward';
  const CRITICAL = 'critical';

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($operation === 'view') {
      if ($account->hasPermission('administer gamify alert')) {
        return AccessResult::allowed();
      }
      if ($this->get('target_uid')->getString() === $account->id() && !!$this->get('inform_user')->getString() && !!$this->get('status')->getString()) {
        return AccessResult::allowed();
      }
      return AccessResult::neutral();
    }
    return parent::access($operation, $account, $return_as_object);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $options = (new AlertTypeOptions)->getPossibleOptions();
    $fields['type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Alert type'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 64)
      ->setDefaultValue(GamifyAlert::NOTE)
      ->setSetting('allowed_values', $options)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['message'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Message'))
      ->setDisplayOptions('form', [
        'type' => 'text_format',
        'settings' => [
          'format' => 'restricted_html',
        ],
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['data_report'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Data Report'))
      ->setDisplayOptions('form', [
        'type' => 'text_format',
        'settings' => [
          'format' => 'data_report',
          'allowed_formats' => ['data_report'],
        ],
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 12,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the gamify alert was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the gamify alert was last edited.'));

    $fields['target_uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Directed to'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['inform_moderator'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Inform moderator'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['requires_confirmation'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Moderator confirmation required'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 22,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['is_confirmed'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Is confirmed'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 22,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['inform_user'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Inform user'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 25,
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

}
