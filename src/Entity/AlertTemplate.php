<?php

namespace Drupal\gamify\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the alert_template entity type.
 *
 * @ConfigEntityType(
 *   id = "alert_template",
 *   label = @Translation("Alert Template"),
 *   label_collection = @Translation("Alert Templates"),
 *   label_singular = @Translation("Alert template"),
 *   label_plural = @Translation("Alert templates"),
 *   label_count = @PluralTranslation(
 *     singular = "@count alert template",
 *     plural = "@count alert templates",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\gamify\AlertTemplateListBuilder",
 *     "form" = {
 *       "add" = "Drupal\gamify\Form\AlertTemplateForm",
 *       "edit" = "Drupal\gamify\Form\AlertTemplateForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "alert_template",
 *   admin_permission = "administer gamify alert",
 *   links = {
 *     "collection" = "/admin/config/workflow/gamify/alert-template",
 *     "add-form" = "/admin/config/workflow/gamify/alert-template/add",
 *     "edit-form" = "/admin/config/workflow/gamify/alert-template/{alert_template}",
 *     "delete-form" = "/admin/config/workflow/gamify/alert-template/{alert_template}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "type",
 *     "message",
 *     "inform_user",
 *     "inform_moderator",
 *     "requires_confirmation"
 *   }
 * )
 */
class AlertTemplate extends ConfigEntityBase implements AlertTemplateInterface {

  /**
   * The alert_template ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The alert_template label.
   *
   * @var string
   */
  protected $label;

  /**
   * The alert_template status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The alert_template description.
   *
   * @var array
   */
  protected $message;

  /**
   * If user will be informed after alert with this template was created.
   *
   * @var bool
   */
  protected bool $inform_user;

  /**
   * If moderators will be informed after alert with this template was created.
   *
   * @var bool
   */
  protected bool $inform_moderator;

  /**
   * If moderator confirmation is required.
   *
   * @var bool
   */
  protected bool $requires_confirmation;
}
