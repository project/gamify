<?php

namespace Drupal\gamify\TypedData\Options;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\rules\TypedData\Options\OptionsProviderBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Options provider for the types of field access to check for.
 */
class AlertTemplateOptions extends OptionsProviderBase implements ContainerInjectionInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a EntityBundleOptions object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleOptions(AccountInterface $account = NULL): array {
    /** @var \Drupal\gamify\Entity\AlertTemplateInterface $templates */
    $templates = $this->entityTypeManager->getStorage('alert_template')
      ->loadMultiple();
    $options = [];
    foreach ($templates as $key => $template) {
      $options[$key] = $template->label();
    }
    return $options;
  }

}
