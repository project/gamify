<?php

namespace Drupal\gamify\TypedData\Options;

use Drupal\Core\Session\AccountInterface;
use Drupal\rules\TypedData\Options\OptionsProviderBase;
use Drupal\gamify\Entity\GamifyAlert as Alert;

/**
 * Options provider for the types of field access to check for.
 */
class AlertTypeOptions extends OptionsProviderBase {

  /**
   * {@inheritdoc}
   */
  public function getPossibleOptions(AccountInterface $account = NULL): array {
    return [
      Alert::REWARD => $this->t('Reward'),
      Alert::NOTE => $this->t('Note'),
      Alert::WARNING => $this->t('Warning'),
      Alert::ERROR => $this->t('Error'),
      Alert::CRITICAL => $this->t('Critical'),
    ];
  }

}
