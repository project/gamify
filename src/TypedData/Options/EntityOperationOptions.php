<?php

namespace Drupal\gamify\TypedData\Options;

use Drupal\Core\Session\AccountInterface;
use Drupal\rules\TypedData\Options\OptionsProviderBase;

/**
 * Options provider for the types of field access to check for.
 */
class EntityOperationOptions extends OptionsProviderBase {

  /**
   * {@inheritdoc}
   */
  public function getPossibleOptions(AccountInterface $account = NULL): array {
    return [
      'create' => $this->t('Create'),
      'view' => $this->t('View'),
      'update' => $this->t('update'),
      'delete' => $this->t('Delete'),
      'revert' => $this->t('Revert'),
      'vote_reward' => $this->t('Vote reward'),
      'ratify_reward' => $this->t('Ratification reward'),
    ];
  }

}
