<?php

namespace Drupal\gamify\TypedData\Options;

use Drupal\Core\Session\AccountInterface;
use Drupal\rules\TypedData\Options\OptionsProviderBase;

/**
 * Options provider for the types of field access to check for.
 */
class AbstractUserOptions extends OptionsProviderBase {


  const CURRENT_USER = 'current_user';
  const ENTITY_CREATOR = 'entity_creator';
  const PARENT_ENTITY_CREATOR = 'parent_entity_creator';
  const INVOLVED_USERS = 'involved_users';

  /**
   * {@inheritdoc}
   */
  public function getPossibleOptions(AccountInterface $account = NULL): array {
    return [
      self::CURRENT_USER => $this->t('Current user'),
      self::ENTITY_CREATOR => $this->t('Entity creator'),
      self::PARENT_ENTITY_CREATOR => $this->t('Parent entity creator'),
      self::INVOLVED_USERS => $this->t('Involved users'),
    ];
  }

}
