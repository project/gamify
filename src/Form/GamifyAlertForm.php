<?php

namespace Drupal\gamify\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the gamify alert entity edit forms.
 */
class GamifyAlertForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    // Advanced tab must be the first, because other fields rely on that.
    if (!isset($form['advanced'])) {
      $adv = [
        'advanced' => [
          '#type' => 'horizontal_tabs',
          '#attributes' => [
            'class' => ['entity-meta'],
          ],
          '#weight' => 99,
        ],
      ];
      $form = array_merge($adv, $form);
    }

    if (isset($form['uid']) || isset($form['created'])) {
      $form['author'] = [
        '#type' => 'details',
        '#title' => $this->t('Authoring information'),
        '#group' => 'advanced',
        '#attributes' => [
          'class' => ['alert-form-author'],
        ],
        '#weight' => 90,
        '#optional' => TRUE,
      ];
    }

    if (isset($form['uid'])) {
      $form['uid']['#group'] = 'author';
    }

    if (isset($form['created'])) {
      $form['created']['#group'] = 'author';
    }

    $form['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings'),
      '#group' => 'advanced',
      '#attributes' => [
        'class' => ['alert-form-settings'],
      ],
      '#weight' => 85,
      '#optional' => TRUE,
    ];

    if (isset($form['target_uid'])) {
      $form['target_uid']['#group'] = 'settings';
    }

    if (isset($form['inform_moderator'])) {
      $form['inform_moderator']['#group'] = 'settings';
    }

    if (isset($form['inform_moderator'])) {
      $form['inform_moderator']['#group'] = 'settings';
    }

    if (isset($form['inform_user'])) {
      $form['inform_user']['#group'] = 'settings';
    }

    if (isset($form['requires_confirmation'])) {
      $form['requires_confirmation']['#group'] = 'settings';
    }

    if (isset($form['is_confirmed'])) {
      $form['is_confirmed']['#group'] = 'settings';
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New gamify alert %label has been created.', $message_arguments));
        $this->logger('gamify')->notice('Created new gamify alert %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The gamify alert %label has been updated.', $message_arguments));
        $this->logger('gamify')->notice('Updated gamify alert %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.gamify_alert.canonical', ['gamify_alert' => $entity->id()]);

    return $result;
  }

}
