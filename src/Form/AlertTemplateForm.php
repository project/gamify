<?php

namespace Drupal\gamify\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\gamify\TypedData\Options\AlertTypeOptions;

/**
 * AlertTemplate form.
 *
 * @property \Drupal\gamify\AlertTemplateInterface $entity
 */
class AlertTemplateForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);

    $form['status'] = [
      '#type' => 'hidden',
      '#value' => '1',
    ];

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the alert template.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\gamify\Entity\AlertTemplate::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $options = (new AlertTypeOptions)->getPossibleOptions();
    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Alert type'),
      '#default_value' => $this->entity->get('type'),
      '#options' => $options,
      '#required' => TRUE,
      '#description' => $this->t('The alert type a kind of error level (note, warning or error).'),
    ];

    $message = $this->entity->get('message');
    $form['message'] = [
      '#type' => 'text_format',
      '#title' => t('Message'),
      '#format' => 'restricted_html',
      '#allowed_formats' => ['restricted_html'],
      '#default_value' => $message['value'] ?? '',
      '#required' => TRUE,
      '#description' => $this->t('Description of the alert template.'),
    ];

    $form['inform_user'] = [
      '#type' => 'checkbox',
      '#title' => t('Inform user'),
      '#default_value' => $this->entity->get('inform_user'),
      '#description' => $this->t('If checked the user will be informed immediately after an alert with this template was created.'),
    ];

    $form['inform_moderator'] = [
      '#type' => 'checkbox',
      '#title' => t('Inform moderator'),
      '#default_value' => $this->entity->get('inform_moderator'),
      '#description' => $this->t('If checked all moderators will be informed immediately after an alert with this template was created.'),
    ];

    $form['requires_confirmation'] = [
      '#type' => 'checkbox',
      '#title' => t('Moderator confirmation required'),
      '#default_value' => $this->entity->get('requires_confirmation'),
      '#description' => $this->t('Alerts may trigger actions. If checked a moderator has to confirm before the action is triggerred.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created new alert template %label.', $message_args)
      : $this->t('Updated alert template %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
