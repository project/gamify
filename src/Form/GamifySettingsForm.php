<?php

namespace Drupal\gamify\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\RoleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Gamify settings for this site.
 */
class GamifySettingsForm extends ConfigFormBase  implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Roles storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $rolesStorage;


  /**
   * Constructs an UpdateSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The email validator.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->rolesStorage = $this->entityTypeManager->getStorage('user_role');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gamify_gamify_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['gamify.settings'];
  }

  /**
   * Get role Options filtered by admin roles.
   *
   * @return array
   */
  protected function getRoleOptions(): array {
    $options = [];
    try {
      /** @var RoleInterface[] $roles */
      $roles = $this->rolesStorage->loadMultiple();
      foreach ($roles as $key => $role) {
        if (str_contains($key, 'admin') || in_array($key, ['anonymous', 'authenticated'])) {
          continue;
        }
        $options[$key] = $role->label();
      }
    } catch(\Exception $e) {}
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['benchmarks'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Benchmarks'),
      '#collapsable' => FALSE,
      '#description' => $this->t('Define a userpoints benchmark, e.g. for beginner role to become a member role. For more advanced role management use rules module.'),
    ];
    $form['benchmarks']['member_min'] = [
      '#type' => 'number',
      '#min' => 0,
      '#step' => 10,
      '#title' => $this->t('Member benchmark'),
      '#default_value' => $this->config('gamify.settings')->get('member_min'),
      '#description' => $this->t('Mininmal userpoints value to receive role below. Set to 0 to disable this feature.'),
    ];

    $form['benchmarks']['role'] = [
      '#type' => 'select',
      '#options' => $this->getRoleOptions(),
      '#title' => $this->t('Member role'),
      '#default_value' => $this->config('gamify.settings')->get('role'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $role_id = $form_state->getValue('role') ?? NULL;
    if (!$role_id || !$this->rolesStorage->load($role_id)) {
      $form_state->setErrorByName('role', $this->t('This is not a valid role.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('gamify.settings')
      ->set('member_min', $form_state->getValue('member_min'))
      ->set('role', $form_state->getValue('role'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
