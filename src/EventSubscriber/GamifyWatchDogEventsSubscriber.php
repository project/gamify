<?php

namespace Drupal\gamify\EventSubscriber;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\gamify\Event\EntityEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\gamify\Traits\GamifyEntityLogTrait;

/**
 * Write log entries to be programmatically validated by gamify module.
 *
 * @see gamify_entity_delete()
 * @see gamify_entity_update()
 * @see gamify_entity_insert()
 */
class GamifyWatchDogEventsSubscriber implements EventSubscriberInterface {

  use GamifyEntityLogTrait;

  /**
   * Drupal\Core\Extension\ModuleHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Drupal\Core\Logger\LoggerChannelInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * Array of entity types that are logged.
   *
   * @var array|null
   */
  protected ?array $loggedEntityTyps = NULL;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The LoggerChannelFactory to open channel for own log entries.
   */
  public function __construct(ModuleHandlerInterface $module_handler, LoggerChannelFactoryInterface $logger_factory) {
    $this->moduleHandler = $module_handler;
    $this->logger = $logger_factory->get('gamify_log');
  }

  /**
   * Invoke all entity types to be logged.
   *
   * @return array|null
   *   Returns an array of entity type ids.
   */
  protected function getLoggedEntityTypes(): ?array {
    if ($this->loggedEntityTyps === NULL) {
      $this->loggedEntityTyps = $this->moduleHandler->invokeAll('gamify_watchdog_entity_types');
    }
    return $this->loggedEntityTyps;
  }

  /**
   * Check if entity type should be logged.
   *
   * @param string $entity_type
   *   Entity type to check.
   *
   * @return bool
   *   Returns TRUE if entity type should be logged.
   */
  protected function isLoggedType(string $entity_type): bool {
    $entity_types = $this->getLoggedEntityTypes();
    return in_array($entity_type, $entity_types);
  }

  /**
   * {@inheritdoc}
   *
   * We can't return the plugin-based events here, but we set up
   * a response to do that at a later stage.
   */
  public static function getSubscribedEvents(): array {
    // Subscribe to kernel request very early.
    $events[EntityEvent::INSERT][] = ['addLogEntry', 100];
    $events[EntityEvent::UPDATE][] = ['addLogEntry', 100];
    $events[EntityEvent::DELETE][] = ['addLogEntry', 100];
    return $events;
  }

  /**
   * Add a watchdog entry for entity operations.
   *
   * @param object $event
   *   Event object.
   * @param string $event_name
   *   Event name.
   */
  public function addLogEntry(object $event, string $event_name): void {
    $entity = $event->getSubject();
    if ($entity instanceof ContentEntityInterface && $this->isLoggedType($entity->getEntityTypeId())) {
      $operation = $this->getOperationFromEventName($event_name);
      $log_id = ($operation) ? $this->buildLogHash($operation, $entity) : NULL;
      if ($log_id) {
        $this->logger->notice($log_id);
      }
    }
  }

}
