<?php

namespace Drupal\gamify;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\gamify\Traits\GamifyEntityLogTrait;
use Drupal\userpoints\Service\UserPointsService;
use Drupal\gamify\TypedData\Options\AbstractUserOptions as UserOpts;

/**
 * Helper class for RulesConditions and RulesActions by query userpoints log.
 *
 * With this class we can check log entries of prior userpoint assignments, and
 * form RulesConditions based on it.
 *
 * @ingroup gamify
 */
class UserPointsLogService {

  use GamifyEntityLogTrait;

  const DEFAULT_POINT_TYPE = 'advancement';

  /**
   * The current active database's master connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * The current active database's master connection.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Drupal\gamify\AbstractUserService definition.
   *
   * @var \Drupal\gamify\AbstractUserService
   */
  protected AbstractUserService $abstractUserService;

  /**
   * Drupal\userpoints\Service\UserPointsService definition.
   *
   * @var \Drupal\userpoints\Service\UserPointsService
   */
  protected UserPointsService $userpointsService;

  /**
   * Drupal\Component\Datetime\TimeInterface definition.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * Constructs a new EvaluatingService object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $database, AbstractUserService $abstract_user_service, UserPointsService $userpoints_service, TimeInterface $time) {
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
    $this->abstractUserService = $abstract_user_service;
    $this->userpointsService = $userpoints_service;
    $this->time = $time;
  }

  /**
   * Executes the Plugin.
   *
   * @param string $type
   *   Original value of an element which is being updated.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that should get an alias.
   * @param string $addressed_user
   *   The abstract user who will receive user points.
   * @param int $user_points
   *   Number of points to assign.
   */
  public function assign(string $type, EntityInterface $entity, string $addressed_user, int $user_points): void {
    $log_msg = "{$this->buildLogHash($type, $entity)} {$entity->label()}";
    foreach ($this->abstractUserService->getUsers($addressed_user, $entity, TRUE) as $user) {
      $this->userpointsService->addPoints($user_points, self::DEFAULT_POINT_TYPE, $user, $log_msg);
    }
  }

  /**
   * Check if max number of repeats is reached, then it fails.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity to check for the provided field.
   * @param string $type
   *   Action type identifier (create, ...) used in the following action.
   * @param int $repeats
   *   The max number of repeats.
   *
   * @return bool
   *   TRUE if the current number of repeats is below or equal as allowed.
   */
  public function evalMaxRepeat(FieldableEntityInterface $entity, string $type, int $repeats): bool {
    $log_hash = $this->getLogHashSearchStr($type, $entity);
    foreach ($this->abstractUserService->getUsers(UserOpts::CURRENT_USER, NULL, TRUE) as $user) {
      $query = $this->queryLog($log_hash, $user);
      return (count($query) <= $repeats);
    }
    return FALSE;
  }

  /**
   * Checks min time to be passed before user gets points for action again.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity to check for the provided field.
   * @param string $type
   *   Action type identifier (create, ...) used in the following action.
   * @param int $time
   *   Defines the minimum time (in seconds) that must have passed before the
   *   user can get points again for the action.
   *
   * @return bool
   *   Returns TRUE:
   *   - if time since last action is greater than given time.
   *   - if it is the first time user executes this action.
   *   Else it returns FALSE.
   */
  public function evalEarliestRepeat(FieldableEntityInterface $entity, string $type, int $time): bool {
    $log_hash = $this->getLogHashSearchStr($type, $entity);
    $users = $this->abstractUserService->getUsers(UserOpts::CURRENT_USER, NULL, TRUE);
    if ($user = reset($users)) {
      $results = $this->queryLog($log_hash, $user);
      $request_time = $this->time->getRequestTime();
      $validates = TRUE;
      foreach ($results as $result) {
        $time_diff = $request_time - $result->revision_timestamp;
        if ($time_diff < $time) {
          $validates = FALSE;
          break;
        }
      }
      return $validates;
    }
    return FALSE;
  }

  /**
   * Method reverts all userpoints given on a log id (or fragment of it).
   *
   * @param string $type
   *   Type of operation.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The target entity.
   * @param string $abstract_user
   *   The abstract user or user group.
   */
  public function revertUserPoints(string $type, EntityInterface $entity, string $abstract_user): void {
    $reverts = [];
    $users = $this->abstractUserService->getUsers($abstract_user, $entity, TRUE);
    $single_user = (count($users) && ($abstract_user !== UserOpts::INVOLVED_USERS)) ? reset($users) : NULL;

    foreach (['create', 'update'] as $operation) {
      $log_hash = $this->getLogHashSearchStr($operation, $entity);

      // Filter by user. Involved don't need a filter. Just taking all.
      $log_entries = $this->queryLog($log_hash, $single_user);

      // Search the query log and spread revert points to each user.
      foreach ($log_entries as $upr) {
        if ($upr->entity_type_id !== 'user') {
          continue;
        }
        if (!isset($reverts[$upr->entity_id])) {
          $reverts[$upr->entity_id] = 0;
        }
        $reverts[$upr->entity_id] += (int) $upr->points ?? 0;
      }
    }

    // Revert user points.
    foreach ($users as $user) {
      $log_id = $this->buildLogHash($type, $entity);
      if ($points = $reverts[$user->id()] ?? NULL) {
        $this->userpointsService->addPoints(-$points, self::DEFAULT_POINT_TYPE, $user, "$log_id Reverting points from prior actions.");
      }
    }
  }

}
