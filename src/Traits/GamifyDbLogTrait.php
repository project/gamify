<?php

namespace Drupal\gamify\Traits;

use Drupal\user\UserInterface;

/**
 * Trait for using Gamify entity log.
 *
 * @ingroup gamify
 */
trait GamifyDbLogTrait {

  /**
   * Query user points revisions for given user and log_id.
   *
   * @param string $queries
   *   The queried log_id.
   * @param \Drupal\user\UserInterface|null $user
   *   The user receiving user points on log_id (Must not be the current user).
   *
   * @return array
   *   Return assoc array of user points revisions keyed by version id.
   */
  protected function queryDbLog(string $queries, UserInterface $user = NULL, int $timestamp = NULL, bool $normalize = FALSE): array {
    $queries = explode('|', $queries);
    $query = $this->database
      ->select('watchdog', 't')
      ->fields('t')
      ->condition('type', "gamify_log")
      ->orderBy('wid');
    foreach ($queries as $str) {
      if (!empty(trim($str))) {
        $operator = 'LIKE';
        if (str_starts_with($str, '^')) {
          $operator = 'NOT LIKE';
          $str = trim($str, '^');
        }
        $query->condition('message', "%{$this->database->escapeLike($str)}%", $operator);
      }
    }
    if ($user) {
      $query->condition('uid', $user->id());
    }
    if ($timestamp) {
      $query->condition('timestamp', $timestamp, '>');
    }
    $results = $query
      ->execute()
      ->fetchAllAssoc('wid');

    if ($normalize) {
      $results = $this->normalizeDbEntry($results);
    }
    return $results;
  }

  /**
   * Get watchdog entries grouped by user.
   *
   * @return array
   *   Returns array keyed by user id contains watchdog entries.
   *
   * @throws \Exception
   */
  public function loadEntries(array $conditions = [], bool $normalize = FALSE): array {
    $query = $this->database->select('watchdog', 'w');
    $query->condition('w.type', 'gamify_log');
    foreach ($conditions as $key => $value) {
      if (is_array($value)) {
        if (!array_key_exists('value', $value) || !array_key_exists('operator', $value)) {
          throw new \Exception('Condition array has to contain a `value` and `operator` key.');
        }
        $query->condition("w.$key", $value['value'], $value['operator']);
      }
      else {
        $query->condition("w.$key", $value);
      }
    }
    $result = $query->fields('w', [
      'wid',
      'uid',
      'severity',
      'type',
      'timestamp',
      'message',
      'variables',
      'link',
    ])->execute();

    $results = $result->fetchAllAssoc('wid');
    if ($normalize) {
      $results = $this->normalizeDbEntry($results);
    }
    return $results;
  }

  /**
   * Transforms query result in a normalized array, equal to userpoints results.
   *
   * @param array $results
   *   Results as StdClass with db-log keys.
   *
   * @return array
   *   Normalized array of arrays with default keys.
   */
  protected function normalizeDbEntry(array $results): array {
    $nomalized = [];
    foreach ($results as $key => $result) {
      $nomalized["wid-$key"] = [
        'src' => 'db_log',
        'log_id' => $result->wid,
        'uid' => $result->uid,
        'log_hash' => $result->message,
        'timestamp' => (int) $result->timestamp,
      ];
    }
    return $nomalized;
  }

}
