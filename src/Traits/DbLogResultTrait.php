<?php

namespace Drupal\gamify\Traits;


/**
 * Trait for using Gamify entity log.
 *
 * @ingroup gamify
 */
trait DbLogResultTrait {

  public function getNewResult(string $trigger, array $entries): ?object {
    if (count($entries)) {
      $result = new \stdClass();
      $result->origin = $trigger;
      $result->interim = ['default' => $entries];
      return $result;
    }
    return NULL;
  }

  /**
   * Get array nesting level of an array.
   *
   * @param object &$result
   *   Some variable assumed to be an array.
   */
  public function clearResult(object &$result): void {
    foreach ($result as $key => $value) {
      if (!in_array($key, ['origin', 'target_uid', 'interim'])) {
        unset($result->$key);
      }
    }
    // Remove all results from interim except of 'default'.
    if (is_array($result->interim) && array_key_exists('default', $result->interim)) {
      $result->interim = ['default' => $result->interim['default']];
    }
  }



  /**
   * Get array nesting level of an interim result array.
   *
   * @param mixed $array
   *   Some variable assumed to be an array.
   *
   * @return int
   *   The array nesting level.
   */
  public function getNestingLevel(mixed $array): int {
    $nesting_level = 0;
    while (is_array($array)) {
      $array = $array[array_key_first($array)];
      $nesting_level++;
    }
    return $nesting_level;
  }

}
