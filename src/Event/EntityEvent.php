<?php

namespace Drupal\gamify\Event;

use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Represent various entity events.
 *
 * @see gamify_entity_presave()
 */
class EntityEvent extends GenericEvent {

  /**
   * Event ids.
   */
  public const INSERT = 'gamify.create';
  public const UPDATE = 'gamify.update';
  public const DELETE = 'gamify.delete';
}
