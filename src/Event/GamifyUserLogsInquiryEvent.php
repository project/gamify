<?php

namespace Drupal\gamify\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\user\UserInterface;

/**
 * Event that is fired on cron run.
 *
 * @see gamify_cron()
 */
class GamifyUserLogsInquiryEvent extends Event {

  const EVENT_NAME = 'gamify_user_logs_inquiry';

  /**
   * The user account.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $user;

  /**
   * When the period since last run begins.
   *
   * @var int
   */
  protected int $lastrun;

  /**
   * The object to collect data during the process.
   *
   * @var object
   */
  protected object $result;

  /**
   * Constructs the object.
   *
   * @param \Drupal\user\UserInterface $user
   *   The account of the user logged in.
   * @param int $lastrun
   *   When the period since last run begins.
   * @param object $result
   *   The results to pass through to condition and action.
   */
  public function __construct(UserInterface $user, int $lastrun, object $result) {
    $this->user = $user;
    $this->lastrun = $lastrun;
    $this->result = $result;
    $this->result->target_uid = $user->id();
  }

}
